import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConozcaPage } from './conozca';

@NgModule({
  declarations: [
    ConozcaPage,
  ],
  imports: [
    IonicPageModule.forChild(ConozcaPage),
  ],
})
export class ConozcaPageModule {}
