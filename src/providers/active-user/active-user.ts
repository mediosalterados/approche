import {Injectable} from '@angular/core';
import {NativeStorage} from '@ionic-native/native-storage/ngx';

import {MenuEsclerosisPage} from '../../pages/menu-esclerosis/menu-esclerosis';
import {MenuAnemiaPage} from '../../pages/menu-anemia/menu-anemia';
import {MenuFibrosisPage} from '../../pages/menu-fibrosis/menu-fibrosis';
import {MenuArtritisPage} from '../../pages/menu-artritis/menu-artritis';
import {MenuHemofiliaPage} from '../../pages/menu-hemofilia/menu-hemofilia';
import {MenuPulmonPage} from '../../pages/menu-pulmon/menu-pulmon';

import {HomePage} from '../../pages/home/home';
/*
  Generated class for the ActiveUserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ActiveUserProvider {
    user: any;
    menuItems: any;
    patologia: any;

    constructor(private nativeStorage: NativeStorage) {
        this.getActiveUser();
    }

    getActiveUser() {
        this.nativeStorage.getItem("currentUser").then(
            data => {
                this.user = data;
                this.menuItems = this.getMenuItems(this.user.patologia.id);
            },
            error => console.error(error)
        );
    }

    getMenuItems(patologia) {
        var mI;
        /*
        Anemia por enfermedad renal crónica
        Artritis Reumatoide
        Cáncer de Pulmón
        Esclerosis
        Fibrosis quística
        Hemofilia
         */

        /*
         * Apoyo Nut
         * Enfermeras
         * Coaching de vida
         * Original de obsequio
         * Centros de infusion
         */
        switch (patologia) {
            case 1:
                mI = [true, true, true, true, false];
                break;
            case 2:
                mI = [true, true, true, true, false];
                break;
            case 3:
                mI = [true, false, true, false, false];
                break;
            case 4:
                mI = [true, false, true, false, false];
                break;
            case 5:
                mI = [true, false, true, false, false];
                break;
            case 6:
                mI = [true, false, true, false, false];
                break;
        }
        return mI;
    }

    getMenuPrincipal() {
        var page;

        if (this.user && this.user !== null) {
            switch (this.user.patologia.id) {
                case 1:
                    page = MenuAnemiaPage;
                    break;
                case 2:
                    page = MenuArtritisPage;
                    break;
                case 3:
                    page = MenuPulmonPage;
                    break;
                case 4:
                    page = MenuEsclerosisPage;
                    break;
                case 5:
                    page = MenuFibrosisPage;
                    break;
                case 6:
                    page = MenuHemofiliaPage;
                    break;
            }
        } else {
            page = HomePage;
        }

        return page;
    }

}