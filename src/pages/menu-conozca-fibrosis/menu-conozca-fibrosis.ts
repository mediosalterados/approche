import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConozcaFibrosisPage } from '../conozca-fibrosis/conozca-fibrosis';
import { InfografiaFibrosisPage } from '../infografia-fibrosis/infografia-fibrosis';
import { EjercicioFibrosisPage } from '../ejercicio-fibrosis/ejercicio-fibrosis';
import { NutricionFibrosisPage } from '../nutricion-fibrosis/nutricion-fibrosis';

/**
 * Generated class for the MenuConozcaFibrosisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-conozca-fibrosis',
  templateUrl: 'menu-conozca-fibrosis.html',
})
export class MenuConozcaFibrosisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuConozcaFibrosisPage');
  }

  goConozcaEnfermedad() {
    this.navCtrl.push(ConozcaFibrosisPage);
  }


  goTipsEjercicios() {
    this.navCtrl.push(EjercicioFibrosisPage);
  }

  goTipsNutricional() {
    this.navCtrl.push(NutricionFibrosisPage);
  }
  goInfografia() {
    this.navCtrl.push(InfografiaFibrosisPage);
  }

}
