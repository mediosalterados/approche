import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ANSchedulePage } from './an-schedule';

@NgModule({
  declarations: [
    ANSchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(ANSchedulePage),
  ],
})
export class ANSchedulePageModule {}
