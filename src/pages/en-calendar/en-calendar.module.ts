import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ENCalendarPage} from './en-calendar';

import {CalendarModule} from "ion2-calendar";

@NgModule({
    declarations: [
        ENCalendarPage,
    ],
    imports: [
        IonicPageModule.forChild(ENCalendarPage),
        CalendarModule
    ],
})
export class ENCalendarPageModule {}
