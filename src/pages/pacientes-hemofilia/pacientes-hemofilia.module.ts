import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PacientesHemofiliaPage } from './pacientes-hemofilia';

@NgModule({
  declarations: [
    PacientesHemofiliaPage,
  ],
  imports: [
    IonicPageModule.forChild(PacientesHemofiliaPage),
  ],
})
export class PacientesHemofiliaPageModule {}
