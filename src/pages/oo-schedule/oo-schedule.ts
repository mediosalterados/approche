import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {OOThanksPage} from '../oo-thanks/oo-thanks';

import {Http} from '@angular/http';
import {NativeStorage} from '@ionic-native/native-storage/ngx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActiveUserProvider} from './../../providers/active-user/active-user';

import {ANIntroPage} from '../an-intro/an-intro';
import {ENIntroPage} from '../en-intro/en-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';
import {OOIntroPage} from '../oo-intro/oo-intro';
import {CIIntroPage} from '../ci-intro/ci-intro';
/**
 * Generated class for the PorTiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-oo-schedule',
    templateUrl: 'oo-schedule.html',
})
export class OOSchedulePage {

    public menuItemsActive: any = [false, false, false, false, false];

    static readonly CREATE_URL = 'http://165.22.227.21/WsAppRoche/web/app.php/api/solicitud/create';

    schedule: 'string';
    date: any;

    solicitud: any;
    user: any;

    stepForm: FormGroup;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private nativeStorage: NativeStorage,
        public http: Http,
        public formBuilder: FormBuilder,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
        this.stepForm = this.createStepForm();
        this.date = navParams.get('date');
    }

    private createStepForm() {
        return this.formBuilder.group({
            schedule: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad OOSchedule');
        this.menuItemsActive = this.activeUser.menuItems;
    }

    goOOThanks() {
        this.user = this.nativeStorage.getItem("currentUser").then(
            data => {
                this.solicitud = {'user': data, 'fecha': this.date, 'horario': this.schedule, 'tipo': 5};
                this.http.post(OOSchedulePage.CREATE_URL, this.solicitud)
                    .map(res => res.json())
                    .subscribe(data => {
                        //console.log(data);
                    });
            },
            error => console.error(error)
        );
        this.navCtrl.setRoot(OOThanksPage);
    }


    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goENIntro() {
        this.navCtrl.push(ENIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

    goOOIntro() {
        this.navCtrl.push(OOIntroPage);
    }
    goCIIntro() {
        this.navCtrl.push(CIIntroPage);
    }

    goMenuPrincipal() {
        this.navCtrl.setRoot(this.activeUser.getMenuPrincipal());
    }
}
