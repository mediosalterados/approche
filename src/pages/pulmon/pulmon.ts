import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PulmonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pulmon',
  templateUrl: 'pulmon.html',
})
export class PulmonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  className: string = 'b-modal d-none'

  ionViewDidLoad() {
    console.log('ionViewDidLoad PulmonPage');
  }

  goHanldeClick() {
    this.className = 'b-modal'
  }

  goHanldeClickNone() {
    this.className = 'b-modal d-none'
  }

}
