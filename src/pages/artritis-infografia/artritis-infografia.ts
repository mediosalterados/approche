import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ArtritisInfografiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-artritis-infografia',
  templateUrl: 'artritis-infografia.html',
})
export class ArtritisInfografiaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  className: string = ' d-none'
  iconTxt1: string = ' d-none'
  iconTxt2: string = ' d-none'
  iconTxt3: string = ' d-none'
  iconTxt4: string = ' d-none'
  iconTxt5: string = ' d-none'
  iconTxt6: string = ' d-none '
  iconTxt7: string = ' d-none'
  iconTxt8: string = ' d-none'

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArtritisInfografiaPage');
  }

  goIcon1() {
  this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = 'd-none',
 this.iconTxt7 = 'd-none',
 this.iconTxt8 = 'd-none',
 this.iconTxt1 = 'icon-info col-10  '
 }

 goIconOff() {
   this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = ' d-none',
 this.iconTxt7 = ' d-none',
 this.iconTxt8 = 'd-none'
 }

 goIcon2() {
 this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = 'd-none',
 this.iconTxt7 = 'd-none',
 this.iconTxt8 = 'd-none',
 this.iconTxt2 = 'icon-info col-10'
 }


  goIcon3() {
 this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = 'd-none',
 this.iconTxt7 = 'd-none',
 this.iconTxt8 = 'd-none',
 this.iconTxt3 = 'icon-info col-10'
 }


  goIcon4() {
 this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = 'd-none',
 this.iconTxt7 = 'd-none',
 this.iconTxt8 = 'd-none',
 this.iconTxt4 = 'icon-info col-10'
 }


  goIcon5() {
 this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = 'd-none',
 this.iconTxt7 = 'd-none',
 this.iconTxt8 = 'd-none',
 this.iconTxt5 = 'icon-info col-10'
 }


  goIcon6() {
 this.iconTxt1 = 'd-none',
 this.iconTxt2 = 'd-none',
 this.iconTxt3 = 'd-none',
 this.iconTxt4 = 'd-none',
 this.iconTxt5 = 'd-none',
 this.iconTxt6 = 'd-none',
 this.iconTxt7 = 'd-none',
 this.iconTxt8 = 'd-none',
 this.iconTxt6 = 'icon-info col-10'
 }

 goIcon7() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none',
this.iconTxt7 = 'd-none',
this.iconTxt8 = 'd-none',
this.iconTxt7 = 'icon-info col-10'
}

goIcon8() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none',
this.iconTxt7 = 'd-none',
this.iconTxt8 = 'd-none',
this.iconTxt8 = 'icon-info col-10'
}

 goHanldeClick() {
   this.className = 'b-modal'
 }

 goHanldeClickNone() {
   this.className = 'b-modal d-none'
 }

}
