import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CISchedulePage } from './ci-schedule';

@NgModule({
  declarations: [
    CISchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(CISchedulePage),
  ],
})
export class CISchedulePageModule {}
