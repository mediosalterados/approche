import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConozcaHemofoliaPage } from './conozca-hemofolia';

@NgModule({
  declarations: [
    ConozcaHemofoliaPage,
  ],
  imports: [
    IonicPageModule.forChild(ConozcaHemofoliaPage),
  ],
})
export class ConozcaHemofoliaPageModule {}
