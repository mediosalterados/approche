import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfografiaFibrosisPage } from './infografia-fibrosis';

@NgModule({
  declarations: [
    InfografiaFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(InfografiaFibrosisPage),
  ],
})
export class InfografiaFibrosisPageModule {}
