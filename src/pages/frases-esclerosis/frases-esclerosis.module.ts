import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrasesEsclerosisPage } from './frases-esclerosis';

@NgModule({
  declarations: [
    FrasesEsclerosisPage,
  ],
  imports: [
    IonicPageModule.forChild(FrasesEsclerosisPage),
  ],
})
export class FrasesEsclerosisPageModule {}
