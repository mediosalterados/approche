import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtristisNutricionalesPage } from './artristis-nutricionales';

@NgModule({
  declarations: [
    ArtristisNutricionalesPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtristisNutricionalesPage),
  ],
})
export class ArtristisNutricionalesPageModule {}
