import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaciAsociaHemofoliaPage } from './paci-asocia-hemofolia';

@NgModule({
  declarations: [
    PaciAsociaHemofoliaPage,
  ],
  imports: [
    IonicPageModule.forChild(PaciAsociaHemofoliaPage),
  ],
})
export class PaciAsociaHemofoliaPageModule {}
