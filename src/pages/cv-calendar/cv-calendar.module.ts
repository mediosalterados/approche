import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CVCalendarPage} from './cv-calendar';

import {CalendarModule} from "ion2-calendar";

@NgModule({
    declarations: [
        CVCalendarPage,
    ],
    imports: [
        IonicPageModule.forChild(CVCalendarPage),
        CalendarModule
    ],
})
export class CVCalendarPageModule {}
