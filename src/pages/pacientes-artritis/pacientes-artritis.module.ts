import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PacientesArtritisPage } from './pacientes-artritis';

@NgModule({
  declarations: [
    PacientesArtritisPage,
  ],
  imports: [
    IonicPageModule.forChild(PacientesArtritisPage),
  ],
})
export class PacientesArtritisPageModule {}
