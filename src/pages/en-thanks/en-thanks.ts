import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ENCalendarPage} from '../en-calendar/en-calendar';

import {ANIntroPage} from '../an-intro/an-intro';
import {ENIntroPage} from '../en-intro/en-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';
import {OOIntroPage} from '../oo-intro/oo-intro';
import {CIIntroPage} from '../ci-intro/ci-intro';

import {ActiveUserProvider} from '../../providers/active-user/active-user';
/**
 * Generated class for the PorTiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-en-thanks',
    templateUrl: 'en-thanks.html',
})
export class ENThanksPage {

    public menuItemsActive: any = [false, false, false, false, false];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad ENThanks');
        this.menuItemsActive = this.activeUser.menuItems;
    }

    goENCalendar() {
        this.navCtrl.push(ENCalendarPage);
    }

    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goENIntro() {
        this.navCtrl.push(ENIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

    goOOIntro() {
        this.navCtrl.push(OOIntroPage);
    }
    goCIIntro() {
        this.navCtrl.push(CIIntroPage);
    }

    goMenuPrincipal() {
        this.navCtrl.setRoot(this.activeUser.getMenuPrincipal());
    }
}
