import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {MenuEsclerosisPage} from '../menu-esclerosis/menu-esclerosis';
import {MenuAnemiaPage} from '../menu-anemia/menu-anemia';
import {MenuFibrosisPage} from '../menu-fibrosis/menu-fibrosis';
import {MenuArtritisPage} from '../menu-artritis/menu-artritis';
import {MenuHemofiliaPage} from '../menu-hemofilia/menu-hemofilia';
import {MenuPulmonPage} from '../menu-pulmon/menu-pulmon';
import {TerminosPage} from '../terminos/terminos'

import {HomePage} from '../home/home';

/**
* Generated class for the MenuPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html',
})
export class MenuPage {
    patology: string;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.patology = navParams.get('patology');
        
         switch (this.patology) {
            case "esclerosis":
                this.navCtrl.push(MenuEsclerosisPage);
                break;
            case "anemia":
                this.navCtrl.push(MenuAnemiaPage);
                break;
            case "fibrosis":
                this.navCtrl.push(MenuFibrosisPage);
                break;
            case "hemofilia":
                this.navCtrl.push(MenuHemofiliaPage);
                break;
            case "artritis":
                this.navCtrl.push(MenuArtritisPage);
                break;
            case "cancer":
                this.navCtrl.push(MenuPulmonPage);
                break;
        }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MenuPage');
    }

    goMenu() {
        this.navCtrl.push(MenuPage);
    }
    goMesclerosis() {
        this.navCtrl.push(MenuEsclerosisPage);
    }
    goManemia() {
        this.navCtrl.push(MenuAnemiaPage);
    }
    goMfibrosis() {
        this.navCtrl.push(MenuFibrosisPage);
    }
    goMhemofilia() {
        this.navCtrl.push(MenuHemofiliaPage);
    }
    goArtritis() {
        this.navCtrl.push(MenuArtritisPage);
    }
    goMenuPulmon() {
        this.navCtrl.push(MenuPulmonPage)
    }
    goTerminos() {
        this.navCtrl.push(TerminosPage)
    }

goHomepage() {
        this.navCtrl.push(HomePage)
    }

}
