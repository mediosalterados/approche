import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtristisEjerciciosPage } from './artristis-ejercicios';

@NgModule({
  declarations: [
    ArtristisEjerciciosPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtristisEjerciciosPage),
  ],
})
export class ArtristisEjerciciosPageModule {}
