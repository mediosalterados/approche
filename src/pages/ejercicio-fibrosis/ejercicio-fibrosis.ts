import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EjercicioFibrosisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ejercicio-fibrosis',
  templateUrl: 'ejercicio-fibrosis.html',
})
export class EjercicioFibrosisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EjercicioFibrosisPage');
  }
  className: string = 'b-modal d-none'

goHanldeClick() {
this.className = 'b-modal'
}

goHanldeClickNone() {
this.className = 'b-modal d-none'
}

}
