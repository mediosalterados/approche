import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaciAsociaPage } from './paci-asocia';

@NgModule({
  declarations: [
    PaciAsociaPage,
  ],
  imports: [
    IonicPageModule.forChild(PaciAsociaPage),
  ],
})
export class PaciAsociaPageModule {}
