import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuAnemiaPage } from './menu-anemia';

@NgModule({
  declarations: [
    MenuAnemiaPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuAnemiaPage),
  ],
})
export class MenuAnemiaPageModule {}
