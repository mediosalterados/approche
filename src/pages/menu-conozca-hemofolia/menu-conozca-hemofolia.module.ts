import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuConozcaHemofoliaPage } from './menu-conozca-hemofolia';

@NgModule({
  declarations: [
    MenuConozcaHemofoliaPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuConozcaHemofoliaPage),
  ],
})
export class MenuConozcaHemofoliaPageModule {}
