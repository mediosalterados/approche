import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HemofiliaPage } from './hemofilia';

@NgModule({
  declarations: [
    HemofiliaPage,
  ],
  imports: [
    IonicPageModule.forChild(HemofiliaPage),
  ],
})
export class HemofiliaPageModule {}
