import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ANThanksPage} from './an-thanks';

@NgModule({
    declarations: [
        ANThanksPage,
    ],
    imports: [
        IonicPageModule.forChild(ANThanksPage),
    ]
})
export class ANThanksPageModule {}
