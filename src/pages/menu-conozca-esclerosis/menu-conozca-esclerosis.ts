import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConozcaPage } from '../conozca/conozca';
import { EsclerosisSistemaNerviosoPage } from '../esclerosis-sistema-nervioso/esclerosis-sistema-nervioso';
import { EsclerosisTipsEjerciciosPage } from '../esclerosis-tips-ejercicios/esclerosis-tips-ejercicios';
import { EsclerosisTipsNutricionalesPage } from '../esclerosis-tips-nutricionales/esclerosis-tips-nutricionales';
import { InfografiaPage } from '../infografia/infografia';


/**
 * Generated class for the MenuConozcaEsclerosisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-conozca-esclerosis',
  templateUrl: 'menu-conozca-esclerosis.html',
})
export class MenuConozcaEsclerosisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuConozcaEsclerosisPage');
  }

  goConozcaEnfermedad() {
    this.navCtrl.push(ConozcaPage);
  }

  goSistemaNervioso() {
    this.navCtrl.push(EsclerosisSistemaNerviosoPage);
  }

  goTipsEjercicios() {
    this.navCtrl.push(EsclerosisTipsEjerciciosPage);
  }

  goTipsNutricional() {
    this.navCtrl.push(EsclerosisTipsNutricionalesPage);
  }
  goInfografia() {
    this.navCtrl.push(InfografiaPage);
  }

}
