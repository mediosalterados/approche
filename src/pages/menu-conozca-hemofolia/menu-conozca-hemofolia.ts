import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InfografiaHemofiliaPage } from '../infografia-hemofilia/infografia-hemofilia';
import { EjercicioHemofiliaPage } from '../ejercicio-hemofilia/ejercicio-hemofilia';
import { NutricionHemofiliaPage } from '../nutricion-hemofilia/nutricion-hemofilia';
import { ConozcaHemofoliaPage } from '../conozca-hemofolia/conozca-hemofolia';

/**
 * Generated class for the MenuConozcaHemofoliaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-conozca-hemofolia',
  templateUrl: 'menu-conozca-hemofolia.html',
})
export class MenuConozcaHemofoliaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuConozcaHemofoliaPage');
  }
   goConozcaEnfermedad() {
    this.navCtrl.push(ConozcaHemofoliaPage);
  }


  goTipsEjercicios() {
    this.navCtrl.push(EjercicioHemofiliaPage);
  }

  goTipsNutricional() {
    this.navCtrl.push(NutricionHemofiliaPage);
  }
  goInfografia() {
    this.navCtrl.push(InfografiaHemofiliaPage);
  }

}
