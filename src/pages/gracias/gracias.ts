import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {MenuEsclerosisPage} from '../menu-esclerosis/menu-esclerosis';
import {MenuAnemiaPage} from '../menu-anemia/menu-anemia';
import {MenuFibrosisPage} from '../menu-fibrosis/menu-fibrosis';
import {MenuArtritisPage} from '../menu-artritis/menu-artritis';
import {MenuHemofiliaPage} from '../menu-hemofilia/menu-hemofilia';
import {MenuPulmonPage} from '../menu-pulmon/menu-pulmon';

/**
 * Generated class for the GraciasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-gracias',
    templateUrl: 'gracias.html',
})
export class GraciasPage {
    patology: string;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.patology = navParams.get('patology');
    }
    goMenu(): void {
        switch (this.patology) {
            case "1":
                this.navCtrl.setRoot(MenuAnemiaPage);
                break;
            case "2":
                this.navCtrl.setRoot(MenuArtritisPage);
                break;
            case "3":
                this.navCtrl.setRoot(MenuPulmonPage);
                break;
            case "4":
                this.navCtrl.setRoot(MenuEsclerosisPage);
                break;
            case "5":
                this.navCtrl.setRoot(MenuFibrosisPage);
                break;
            case "6":
                this.navCtrl.setRoot(MenuHemofiliaPage);
                break;
        }
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad GraciasPage');
    }

}
