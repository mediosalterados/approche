import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcompanaPage } from './acompana';

@NgModule({
  declarations: [
    AcompanaPage,
  ],
  imports: [
    IonicPageModule.forChild(AcompanaPage),
  ],
})
export class AcompanaPageModule {}
