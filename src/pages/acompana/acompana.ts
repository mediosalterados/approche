import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FrasesEsclerosisPage } from '../frases-esclerosis/frases-esclerosis';

/**
 * Generated class for the AcompanaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-acompana',
  templateUrl: 'acompana.html',
})
export class AcompanaPage {
  items = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = [
      {
        'mini': 'frase1',
        'img': 'frase1_grande',
        
      },
      {
        'mini': 'frase2',
        'img': 'frase2_grande',
       
      },
]
  }
   goFrasesEs(item) {
    this.navCtrl.push(FrasesEsclerosisPage, { item: item });
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad AcompanaPage');
  }

}
