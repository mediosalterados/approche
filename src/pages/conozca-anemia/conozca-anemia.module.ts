import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConozcaAnemiaPage } from './conozca-anemia';

@NgModule({
  declarations: [
    ConozcaAnemiaPage,
  ],
  imports: [
    IonicPageModule.forChild(ConozcaAnemiaPage),
  ],
})
export class ConozcaAnemiaPageModule {}
