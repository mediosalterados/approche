import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EjercicioFibrosisPage } from './ejercicio-fibrosis';

@NgModule({
  declarations: [
    EjercicioFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(EjercicioFibrosisPage),
  ],
})
export class EjercicioFibrosisPageModule {}
