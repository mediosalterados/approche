import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuFibrosisPage } from './menu-fibrosis';

@NgModule({
  declarations: [
    MenuFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuFibrosisPage),
  ],
})
export class MenuFibrosisPageModule {}
