import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuConozcaArtritisPage } from './menu-conozca-artritis';

@NgModule({
  declarations: [
    MenuConozcaArtritisPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuConozcaArtritisPage),
  ],
})
export class MenuConozcaArtritisPageModule {}
