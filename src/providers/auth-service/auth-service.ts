import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

    // Change to this http://ed43bb3b.ngrok.io/api/login
    static readonly LOGIN_URL = 'http://165.22.227.21/WsAppRoche/web/app.php/api/user/usuario';
    // Change to this http://ed43bb3b.ngrok.io/api/register
    static readonly REGISTER_URL = 'http://165.22.227.21/WsAppRoche/web/app.php/api/user/create';
    access: boolean;
    token: string;
    valid_register: boolean;

    user: object;

    constructor(public http: Http) {}

    public getUserData() {
        return this.user;
    }

    // Login
    public login(credentials) {
        if (credentials.email === null || credentials.password === null) {
            return Observable.throw("Please insert credentials.");
        } else {
            return Observable.create(observer => {

                this.http.post(AuthServiceProvider.LOGIN_URL, credentials)
                    .map(res => res.json())
                    .subscribe(data => {
                        //console.log(data);
                        this.valid_register = data.status == "ok" ? true : false;
                        this.user = data.response.user;
                        observer.next(data);
                        observer.complete();
                    });

            }, err => console.error(err));
        }
    }

    // Register
    public register(credentials) {
        if (credentials.name === null || credentials.email === null || credentials.password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            return Observable.create(observer => {
                this.http.post(AuthServiceProvider.REGISTER_URL, credentials)
                    .map(res => res.json())
                    .subscribe(data => { 
                        if(data.status == "ok" ){
                            this.user = data.response.user;
                            this.valid_register = true;
                        }else{
                            this.valid_register = false;
                        }
                        observer.next(data);
                        observer.complete();
                    });

            });
        }
    }

    // Get Token
    public getToken() {
        return this.token;
    }

    public getValidRegister() {
        return this.valid_register;
    }



    // Logout
    public logout() {
        return Observable.create(observer => {
            observer.next(true);
            observer.complete();
        });
    }

}