import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ENIntroPage } from './en-intro';

@NgModule({
  declarations: [
    ENIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(ENIntroPage),
  ],
})
export class ENIntroPageModule {}
