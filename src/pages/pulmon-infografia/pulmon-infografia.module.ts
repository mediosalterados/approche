import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PulmonInfografiaPage } from './pulmon-infografia';

@NgModule({
  declarations: [
    PulmonInfografiaPage,
  ],
  imports: [
    IonicPageModule.forChild(PulmonInfografiaPage),
  ],
})
export class PulmonInfografiaPageModule {}
