import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {OOCalendarPage} from './oo-calendar';

import {CalendarModule} from "ion2-calendar";

@NgModule({
    declarations: [
        OOCalendarPage,
    ],
    imports: [
        IonicPageModule.forChild(OOCalendarPage),
        CalendarModule
    ],
})
export class OOCalendarPageModule {}
