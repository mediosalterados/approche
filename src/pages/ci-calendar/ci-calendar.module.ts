import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CICalendarPage} from './ci-calendar';

import {CalendarModule} from "ion2-calendar";

@NgModule({
    declarations: [
        CICalendarPage,
    ],
    imports: [
        IonicPageModule.forChild(CICalendarPage),
        CalendarModule
    ],
})
export class CICalendarPageModule {}
