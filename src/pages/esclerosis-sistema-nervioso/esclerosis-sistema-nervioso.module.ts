import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EsclerosisSistemaNerviosoPage } from './esclerosis-sistema-nervioso';

@NgModule({
  declarations: [
    EsclerosisSistemaNerviosoPage,
  ],
  imports: [
    IonicPageModule.forChild(EsclerosisSistemaNerviosoPage),
  ],
})
export class EsclerosisSistemaNerviosoPageModule {}
