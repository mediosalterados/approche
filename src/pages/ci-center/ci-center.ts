import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {CICalendarPage} from '../ci-calendar/ci-calendar';

import {Http} from '@angular/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {CIIntroPage} from '../ci-intro/ci-intro';
import {ANIntroPage} from '../an-intro/an-intro';
import {ENIntroPage} from '../en-intro/en-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';
import {OOIntroPage} from '../oo-intro/oo-intro';

import {ActiveUserProvider} from './../../providers/active-user/active-user';
/**
 * Generated class for the PorTiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-ci-center',
    templateUrl: 'ci-center.html',
})
export class CICenterPage {

    public menuItemsActive: any = [false, false, false, false, false];

    state: 'string';
    public centrosInfusion: any;
    centroInfusion: any;
    stepForm: FormGroup;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public http: Http,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
        this.state = navParams.get('state');
        this.stepForm = this.createStepForm();

        this.http.get("http://165.22.227.21/WsAppRoche/web/app.php/api/centro-infusion/estado", {params: {'state': this.state}})
            .map(res => res.json())
            .subscribe(response => {
                //console.log(response);
                this.centrosInfusion = response.data.centrosInfusion;
                console.log(this.centrosInfusion);
            });
    }

    private createStepForm() {
        return this.formBuilder.group({
            centroInfusion: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad CIIntro');
        this.menuItemsActive = this.activeUser.menuItems;
    }

    goCICalendar() {
        this.navCtrl.push(CICalendarPage, {centroInfusion: this.centroInfusion});
    }


    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goENIntro() {
        this.navCtrl.push(ENIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

    goOOIntro() {
        this.navCtrl.push(OOIntroPage);
    }
    goCIIntro() {
        this.navCtrl.push(CIIntroPage);
    }

    goMenuPrincipal() {
        this.navCtrl.setRoot(this.activeUser.getMenuPrincipal());
    }
}
