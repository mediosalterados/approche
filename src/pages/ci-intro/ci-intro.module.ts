import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CIIntroPage } from './ci-intro';

@NgModule({
  declarations: [
    CIIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(CIIntroPage),
  ],
})
export class CIIntroPageModule {}
