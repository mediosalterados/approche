import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuPulmonPage } from './menu-pulmon';

@NgModule({
  declarations: [
    MenuPulmonPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuPulmonPage),
  ],
})
export class MenuPulmonPageModule {}
