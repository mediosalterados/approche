import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ArtristisNutricionalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-artristis-nutricionales',
  templateUrl: 'artristis-nutricionales.html',
})
export class ArtristisNutricionalesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  className: string = 'b-modal d-none'

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArtristisNutricionalesPage');
  }

  goHanldeClick() {
    this.className = 'b-modal'
  }

  goHanldeClickNone() {
    this.className = 'b-modal d-none'
  }

}
