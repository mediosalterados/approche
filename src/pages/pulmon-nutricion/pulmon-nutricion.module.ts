import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PulmonNutricionPage } from './pulmon-nutricion';

@NgModule({
  declarations: [
    PulmonNutricionPage,
  ],
  imports: [
    IonicPageModule.forChild(PulmonNutricionPage),
  ],
})
export class PulmonNutricionPageModule {}
