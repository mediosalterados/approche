import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OOThanksPage } from './oo-thanks';

@NgModule({
  declarations: [
    OOThanksPage,
  ],
  imports: [
    IonicPageModule.forChild(OOThanksPage),
  ],
})
export class OOThanksPageModule {}
