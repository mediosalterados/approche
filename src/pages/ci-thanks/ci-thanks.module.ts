import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CIThanksPage } from './ci-thanks';

@NgModule({
  declarations: [
    CIThanksPage,
  ],
  imports: [
    IonicPageModule.forChild(CIThanksPage),
  ],
})
export class CIThanksPageModule {}
