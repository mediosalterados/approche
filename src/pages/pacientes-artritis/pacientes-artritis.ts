import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaciAsociaArtritisPage } from '../paci-asocia-artritis/paci-asocia-artritis';

/**
 * Generated class for the PacientesArtritisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pacientes-artritis',
  templateUrl: 'pacientes-artritis.html',
})
export class PacientesArtritisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacientesArtritisPage');
  }
     goPaciAsocia(){
    this.navCtrl.push(PaciAsociaArtritisPage);
}

}
