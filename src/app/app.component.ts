import {Component, ViewChild, Inject, forwardRef} from '@angular/core';
import {Nav, Platform, MenuController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {HomePage} from '../pages/home/home';
//import {MenuAnemiaPage} from '../pages/menu-anemia/menu-anemia';
//import {MenuEsclerosisPage} from '../pages/menu-esclerosis/menu-esclerosis';
//import {MenuFibrosisPage} from '../pages/menu-fibrosis/menu-fibrosis';
//import {MenuArtritisPage} from '../pages/menu-artritis/menu-artritis';
//import {MenuHemofiliaPage} from '../pages/menu-hemofilia/menu-hemofilia';
//import {MenuPage} from '../pages/menu/menu';
import {TrmPage} from '../pages/trm/trm';
import {ActiveUserProvider} from '../providers/active-user/active-user';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    text: string = '';
    rootPage: any = HomePage;

    menuPrincipal: any;

    pages: Array<{title: string, icon: string, component: any}>;

    constructor(
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public menuCtrl: MenuController,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.initializeApp();

        // used for an example of ngFor and navigation

        platform.registerBackButtonAction(() => {

        });

    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.menuPrincipal = this.goMenuPrincipal();
            this.pages = [
                //            {title: 'Inicio', icon: 'i-h', component: MenuPage},
                //            {title: 'Artritis Reumatoide(Artritis idiopática juvenil (AIJ), Artritis Idiopática Poliarticular, Artritis Reumatoide Temprana)', icon: 'i-1', component: MenuArtritisPage},
                //            {title: 'Anemia por Enfermedad Renal Crónica', icon: 'i-2', component: MenuAnemiaPage},
                //            {title: 'Fibrosis Quística', icon: 'i-4', component: MenuFibrosisPage},
                //            {title: 'Hemofilia', icon: 'i-5', component: MenuHemofiliaPage},
                //            {title: 'Esclerosis Múltiple( Primaria progresiva, Remitente recurrente o secundaria progresiva)', icon: 'i-6', component: MenuEsclerosisPage},
                {title: 'Inicio', icon: 'i-h', component: this.menuPrincipal},
                {title: 'Cerrar sesión', icon: 'fin', component: HomePage}
            ];

        });
    }
   
    updatePages(){
        this.menuPrincipal = this.goMenuPrincipal();
        this.pages = [
                {title: 'Inicio', icon: 'i-h', component: this.menuPrincipal},
                {title: 'Cerrar sesión', icon: 'fin', component: HomePage}
            ];
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    goTrm() {
        this.nav.setRoot(TrmPage)
    }

    rightMenuClick(text) {
        this.text = text;
    }

    goMenuPrincipal() {
        //        console.log(this.activeUser.getMenuPrincipal());
        return this.activeUser.getMenuPrincipal();
    }

}