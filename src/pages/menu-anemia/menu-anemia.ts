import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ContactoPage} from '../contacto/contacto';
import {PorTiPage} from '../por-ti/por-ti';
import {AcompanaPage} from '../acompana/acompana';
import {PacientesAnemiaPage} from '../pacientes-anemia/pacientes-anemia';
import {MenuConozcaAnemiaPage} from '../menu-conozca-anemia/menu-conozca-anemia'

import {ActiveUserProvider} from '../../providers/active-user/active-user';

import {ANIntroPage} from '../an-intro/an-intro';
import {ENIntroPage} from '../en-intro/en-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';
import {OOIntroPage} from '../oo-intro/oo-intro';
import {CIIntroPage} from '../ci-intro/ci-intro';

import {MyApp} from '../../app/app.component';
/**
 * Generated class for the MenuAnemiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-menu-anemia',
    templateUrl: 'menu-anemia.html',
})
export class MenuAnemiaPage {


    constructor(
        @Inject(forwardRef(() => MyApp)) private app: MyApp,
        public navCtrl: NavController,
        public navParams: NavParams,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
    }

    ionViewDidLoad() {
        this.app.updatePages();
        //console.log('ionViewDidLoad MenuAnemiaPage');
    }
    goContacto() {
        this.navCtrl.push(ContactoPage);
    }
    goPorti() {
        this.navCtrl.push(PorTiPage);
    }

    goMenuEnfermedad() {
        this.navCtrl.push(MenuConozcaAnemiaPage);
    }
    goAcompana() {
        this.navCtrl.push(AcompanaPage);
    }
    goPacientes() {
        this.navCtrl.push(PacientesAnemiaPage);
    }


    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goENIntro() {
        this.navCtrl.push(ENIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

    goOOIntro() {
        this.navCtrl.push(OOIntroPage);
    }
    goCIIntro() {
        this.navCtrl.push(CIIntroPage);
    }

}
