import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PacientesFibrosisPage } from './pacientes-fibrosis';

@NgModule({
  declarations: [
    PacientesFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(PacientesFibrosisPage),
  ],
})
export class PacientesFibrosisPageModule {}
