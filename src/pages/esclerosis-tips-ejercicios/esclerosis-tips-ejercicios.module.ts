import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EsclerosisTipsEjerciciosPage } from './esclerosis-tips-ejercicios';

@NgModule({
  declarations: [
    EsclerosisTipsEjerciciosPage,
  ],
  imports: [
    IonicPageModule.forChild(EsclerosisTipsEjerciciosPage),
  ],
})
export class EsclerosisTipsEjerciciosPageModule {}
