import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CVIntroPage } from './cv-intro';

@NgModule({
  declarations: [
    CVIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(CVIntroPage),
  ],
})
export class CVIntroPageModule {}
