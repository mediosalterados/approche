import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConozcaAnemiaPage } from '../conozca-anemia/conozca-anemia';
import { InfografiaAnemiaPage } from '../infografia-anemia/infografia-anemia';
/**
 * Generated class for the MenuConozcaAnemiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-conozca-anemia',
  templateUrl: 'menu-conozca-anemia.html',
})
export class MenuConozcaAnemiaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuConozcaAnemiaPage');
  }
   goConozcaEnfermedad() {
    this.navCtrl.push(ConozcaAnemiaPage);
  }
  goInfografia() {
    this.navCtrl.push(InfografiaAnemiaPage);
  }

}
