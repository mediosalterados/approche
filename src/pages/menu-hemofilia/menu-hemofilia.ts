import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ContactoPage} from '../contacto/contacto';
import {PorTiPage} from '../por-ti/por-ti';
import {AcompanaPage} from '../acompana/acompana';
import {PacientesHemofiliaPage} from '../pacientes-hemofilia/pacientes-hemofilia';
import {MenuConozcaHemofoliaPage} from '../menu-conozca-hemofolia/menu-conozca-hemofolia';
import {MenuPage} from '../menu/menu';

import {ActiveUserProvider} from '../../providers/active-user/active-user';

import {ANIntroPage} from '../an-intro/an-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';

import {MyApp} from '../../app/app.component';
/**
 * Generated class for the MenuHemofiliaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-menu-hemofilia',
    templateUrl: 'menu-hemofilia.html',
})
export class MenuHemofiliaPage {

    constructor(
        @Inject(forwardRef(() => MyApp)) private app: MyApp,
        public navCtrl: NavController,
        public navParams: NavParams,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
    }

    ionViewDidLoad() {
        this.app.updatePages();
        //console.log('ionViewDidLoad MenuHemofiliaPage');
    }
    goContacto() {
        this.navCtrl.push(ContactoPage);
    }
    goPorti() {
        this.navCtrl.push(PorTiPage);
    }

    goMenuEnfermedad() {
        this.navCtrl.push(MenuConozcaHemofoliaPage);
    }
    goAcompana() {
        this.navCtrl.push(AcompanaPage);
    }

    goPacientesHemofilia() {
        this.navCtrl.push(PacientesHemofiliaPage);
    }
    goMenu(): void {
        this.navCtrl.push(MenuPage);
    }

    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }
}
