import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ANCalendarPage} from './an-calendar';

import {CalendarModule} from "ion2-calendar";

@NgModule({
    declarations: [
        ANCalendarPage,
    ],
    imports: [
        IonicPageModule.forChild(ANCalendarPage),
        CalendarModule
    ],
})
export class ANCalendarPageModule {}
