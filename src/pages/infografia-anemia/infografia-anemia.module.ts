import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfografiaAnemiaPage } from './infografia-anemia';

@NgModule({
  declarations: [
    InfografiaAnemiaPage,
  ],
  imports: [
    IonicPageModule.forChild(InfografiaAnemiaPage),
  ],
})
export class InfografiaAnemiaPageModule {}
