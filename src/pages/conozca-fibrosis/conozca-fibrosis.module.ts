import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConozcaFibrosisPage } from './conozca-fibrosis';

@NgModule({
  declarations: [
    ConozcaFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(ConozcaFibrosisPage),
  ],
})
export class ConozcaFibrosisPageModule {}
