import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
 

/**
 * Generated class for the ContactoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html',
})
export class ContactoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private emailComposer: EmailComposer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactoPage');
  }
   sendEmail() {
    let email = {
      to: 'mexico.info@roche.com',
      attachments: [
      ],
      subject: 'Eventos adversos',
      body: 'NOMBRE DEL PACIENTE:<br><br> MEDIAMENTO:<br><br> ¿QUÉ PASÓ?:<br><br> CUÁNDO PASÓ:',
  
      isHtml: true
    };
 
    this.emailComposer.open(email);
  }

}
