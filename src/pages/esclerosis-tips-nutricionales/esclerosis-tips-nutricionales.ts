import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EsclerosisTipsNutricionalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esclerosis-tips-nutricionales',
  templateUrl: 'esclerosis-tips-nutricionales.html',
})
export class EsclerosisTipsNutricionalesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  className: string = 'b-modal d-none'

  ionViewDidLoad() {
    console.log('ionViewDidLoad EsclerosisTipsNutricionalesPage');
  }

  goHanldeClick() {
    this.className = 'b-modal'
  }

  goHanldeClickNone() {
    this.className = 'b-modal d-none'
  }

}
