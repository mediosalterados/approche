import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuHemofiliaPage } from './menu-hemofilia';

@NgModule({
  declarations: [
    MenuHemofiliaPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuHemofiliaPage),
  ],
})
export class MenuHemofiliaPageModule {}
