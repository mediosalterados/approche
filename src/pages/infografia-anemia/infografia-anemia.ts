import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
* Generated class for the InfografiaAnemiaPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
selector: 'page-infografia-anemia',
templateUrl: 'infografia-anemia.html',
})
export class InfografiaAnemiaPage {

constructor(public navCtrl: NavController, public navParams: NavParams) {
}
className: string = ' d-none'
iconTxt1: string = ' d-none'
iconTxt2: string = ' d-none'
iconTxt3: string = ' d-none'
iconTxt4: string = ' d-none'
iconTxt5: string = ' d-none'
iconTxt6: string = ' d-none'
iconTxt7: string = ' d-none'
iconTxt8: string = ' d-none'
iconTxt9: string = ' d-none'
 

ionViewDidLoad() {
console.log('ionViewDidLoad InfografiaAnemiaPage');
}
goIcon1() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt1 = 'icon-info icon-1-i col-5'

}

goIconOff() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt9 = 'd-none',
this.iconTxt6 = ' d-none'
}

goIcon2() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt2 = 'icon-info icon-2-i col-5'
}

 
goIcon3() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt3 = 'icon-info icon-3-i col-5'
}

 
goIcon4() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', this.iconTxt7 = 'd-none', this.iconTxt8 = 'd-none', this.iconTxt9 = 'd-none',
this.iconTxt4 = 'icon-info icon-4-i col-5'
}


goIcon5() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', this.iconTxt7 = 'd-none', this.iconTxt8 = 'd-none', this.iconTxt9 = 'd-none',
this.iconTxt5 = 'icon-info icon-5-i col-5'
}

  
goIcon6() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt6 = 'icon-info icon-6-i col-5'
}
goIcon7() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt7 = 'icon-info icon-7-i col-5'
}
goIcon8() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt8 = 'icon-info icon-8-i col-5'
}
goIcon9() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt9 = 'b-modal'
}

goHanldeClick() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.className = 'b-modal'
}

goHanldeClickNone() {
this.iconTxt1 = 'd-none',
this.iconTxt2 = 'd-none',
this.iconTxt3 = 'd-none',
this.iconTxt4 = 'd-none',
this.iconTxt5 = 'd-none',
this.iconTxt6 = 'd-none', 
this.iconTxt7 = 'd-none', 
this.iconTxt8 = 'd-none', 
this.iconTxt9 = 'd-none',
this.iconTxt9 = 'd-none',
this.className =  'd-none'
}
 
gocerrar(){
this.iconTxt9 = 'd-none'
}   

}
