import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutricionFibrosisPage } from './nutricion-fibrosis';

@NgModule({
  declarations: [
    NutricionFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(NutricionFibrosisPage),
  ],
})
export class NutricionFibrosisPageModule {}
