import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EjercicioHemofiliaPage } from './ejercicio-hemofilia';

@NgModule({
  declarations: [
    EjercicioHemofiliaPage,
  ],
  imports: [
    IonicPageModule.forChild(EjercicioHemofiliaPage),
  ],
})
export class EjercicioHemofiliaPageModule {}
