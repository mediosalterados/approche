import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuEsclerosisPage } from './menu-esclerosis';

@NgModule({
  declarations: [
    MenuEsclerosisPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuEsclerosisPage),
  ],
})
export class MenuEsclerosisPageModule {}
