import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaciAsociaAnemiaPage } from './paci-asocia-anemia';

@NgModule({
  declarations: [
    PaciAsociaAnemiaPage,
  ],
  imports: [
    IonicPageModule.forChild(PaciAsociaAnemiaPage),
  ],
})
export class PaciAsociaAnemiaPageModule {}
