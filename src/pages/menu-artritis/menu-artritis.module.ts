import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuArtritisPage } from './menu-artritis';

@NgModule({
  declarations: [
    MenuArtritisPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuArtritisPage),
  ],
})
export class MenuArtritisPageModule {}
