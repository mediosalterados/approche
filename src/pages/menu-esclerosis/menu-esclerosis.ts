import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ContactoPage} from '../contacto/contacto';
import {PorTiPage} from '../por-ti/por-ti';
import {AcompanaPage} from '../acompana/acompana';
import {PacientesPage} from '../pacientes/pacientes';
import {MenuConozcaEsclerosisPage} from '../menu-conozca-esclerosis/menu-conozca-esclerosis';
import {MenuPage} from '../menu/menu';

import {ActiveUserProvider} from '../../providers/active-user/active-user';

import {ANIntroPage} from '../an-intro/an-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';

import {MyApp} from '../../app/app.component';
/**
* Generated class for the MenuEsclerosisPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
    selector: 'page-menu-esclerosis',
    templateUrl: 'menu-esclerosis.html',
})
export class MenuEsclerosisPage {

    constructor(
        @Inject(forwardRef(() => MyApp)) private app: MyApp,
        public navCtrl: NavController,
        public navParams: NavParams,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
    }

    ionViewDidLoad() {
        this.app.updatePages();
        //console.log('ionViewDidLoad MenuEsclerosisPage');
    }

    goContacto() {
        this.navCtrl.push(ContactoPage);
    }
    goPorti() {
        this.navCtrl.push(PorTiPage);
    }

    goMenuEnfermedad() {
        this.navCtrl.push(MenuConozcaEsclerosisPage);
    }
    goAcompana() {
        this.navCtrl.push(AcompanaPage);
    }
    goPacientes() {
        this.navCtrl.push(PacientesPage);
    }
    //  goAsoci(){
    //     this.navCtrl.push(AsociacionPage);
    // }

    goMenu(): void {
        this.navCtrl.push(MenuPage);
    }

    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

}
