import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OOSchedulePage } from './oo-schedule';

@NgModule({
  declarations: [
    OOSchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(OOSchedulePage),
  ],
})
export class OOSchedulePageModule {}
