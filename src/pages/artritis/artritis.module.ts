import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtritisPage } from './artritis';

@NgModule({
  declarations: [
    ArtritisPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtritisPage),
  ],
})
export class ArtritisPageModule {}
