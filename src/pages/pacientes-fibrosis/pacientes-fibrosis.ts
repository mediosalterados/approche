import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaciAsociaFibrosisPage } from '../paci-asocia-fibrosis/paci-asocia-fibrosis';

/**
 * Generated class for the PacientesFibrosisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pacientes-fibrosis',
  templateUrl: 'pacientes-fibrosis.html',
})
export class PacientesFibrosisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacientesFibrosisPage');
  }
  goPaciAsocia(){
    this.navCtrl.push(PaciAsociaFibrosisPage);
}

}
