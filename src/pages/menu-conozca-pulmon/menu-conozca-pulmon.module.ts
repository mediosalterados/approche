import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuConozcaPulmonPage } from './menu-conozca-pulmon';

@NgModule({
  declarations: [
    MenuConozcaPulmonPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuConozcaPulmonPage),
  ],
})
export class MenuConozcaPulmonPageModule {}
