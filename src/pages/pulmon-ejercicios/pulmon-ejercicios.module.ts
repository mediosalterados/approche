import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PulmonEjerciciosPage } from './pulmon-ejercicios';

@NgModule({
  declarations: [
    PulmonEjerciciosPage,
  ],
  imports: [
    IonicPageModule.forChild(PulmonEjerciciosPage),
  ],
})
export class PulmonEjerciciosPageModule {}
