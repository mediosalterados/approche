import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
//import {MenuPage} from '../menu/menu'
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

import {NativeStorage} from '@ionic-native/native-storage/ngx';

import {MenuEsclerosisPage} from '../menu-esclerosis/menu-esclerosis';
import {MenuAnemiaPage} from '../menu-anemia/menu-anemia';
import {MenuFibrosisPage} from '../menu-fibrosis/menu-fibrosis';
import {MenuArtritisPage} from '../menu-artritis/menu-artritis';
import {MenuHemofiliaPage} from '../menu-hemofilia/menu-hemofilia';
import {MenuPulmonPage} from '../menu-pulmon/menu-pulmon';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    loginSuccess = false;
    loginCredentials = {username: '', password: ''};
    myForm: FormGroup;
    patology: string;
    user: any;
    patologia: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        private auth: AuthServiceProvider,
        private alertCtrl: AlertController,
        private nativeStorage: NativeStorage
    ) {
        this.myForm = this.createMyForm();
    }


    saveData() {
        this.auth.login(this.loginCredentials).subscribe(success => {

            if (success && this.auth.getValidRegister()) {
                this.user = this.auth.getUserData();

                this.nativeStorage.setItem("currentUser", this.user);


                this.goMenu();
            } else {
                this.showPopup("Error", "Usuario o contraseña no son correctos");
            }
        },
            error => {
                // this.showPopup("Error", error);
            });

        //console.log(this.myForm.value);
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad LoginPage');
    }



    private createMyForm() {
        return this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    goMenu() {
        //console.log("gomenulogin");
        switch (this.user.patologia.id) {
            case 1:
                this.navCtrl.setRoot(MenuAnemiaPage);
                break;
            case 2:
                this.navCtrl.setRoot(MenuArtritisPage);
                break;
            case 3:
                this.navCtrl.setRoot(MenuPulmonPage);
                break;
            case 4:
                this.navCtrl.setRoot(MenuEsclerosisPage);
                break;
            case 5:
                this.navCtrl.setRoot(MenuFibrosisPage);
                break;
            case 6:
                this.navCtrl.setRoot(MenuHemofiliaPage);
                break;
        }
        //console.log("gomenuloginhow");
    }

    showPopup(title, text) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        if (this.loginSuccess) {
                            this.goMenu();
                        }
                    }
                }
            ]
        });
        alert.present();
    }


}
