import {Component, Inject, forwardRef} from '@angular/core';
import { CalendarComponentOptions } from 'ion2-calendar';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ANSchedulePage} from '../an-schedule/an-schedule';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ANIntroPage} from '../an-intro/an-intro';
import {ENIntroPage} from '../en-intro/en-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';
import {OOIntroPage} from '../oo-intro/oo-intro';
import {CIIntroPage} from '../ci-intro/ci-intro';

import {ActiveUserProvider} from './../../providers/active-user/active-user';
/**
 * Generated class for the PorTiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-an-calendar',
    templateUrl: 'an-calendar.html',
})
export class ANCalendarPage {

    public menuItemsActive: any = [false, false, false, false, false];
    date: string;
    type: 'string';

    stepForm: FormGroup;

     options: CalendarComponentOptions = {
    monthPickerFormat: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
    weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
  };

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
        this.stepForm = this.createStepForm();
    }

    private createStepForm() {
        return this.formBuilder.group({
            date: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad ANCalendar');
        this.menuItemsActive = this.activeUser.menuItems;
    }

    onChange($event) {
        //console.log($event);
    }

    goANSchedule() {
        this.navCtrl.push(ANSchedulePage, {date: this.date});
    }

    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goENIntro() {
        this.navCtrl.push(ENIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

    goOOIntro() {
        this.navCtrl.push(OOIntroPage);
    }
    goCIIntro() {
        this.navCtrl.push(CIIntroPage);
    }

    goMenuPrincipal() {
        this.navCtrl.setRoot(this.activeUser.getMenuPrincipal());
    }
}
