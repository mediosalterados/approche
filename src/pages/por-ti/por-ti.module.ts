import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PorTiPage } from './por-ti';

@NgModule({
  declarations: [
    PorTiPage,
  ],
  imports: [
    IonicPageModule.forChild(PorTiPage),
  ],
})
export class PorTiPageModule {}
