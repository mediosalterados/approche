import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaciAsociaHemofoliaPage } from '../paci-asocia-hemofolia/paci-asocia-hemofolia';

/**
 * Generated class for the PacientesHemofiliaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pacientes-hemofilia',
  templateUrl: 'pacientes-hemofilia.html',
})
export class PacientesHemofiliaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacientesHemofiliaPage');
  }
   goPaciAsocia(){
    this.navCtrl.push(PaciAsociaHemofoliaPage);
}

}
