import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaciAsociaFibrosisPage } from './paci-asocia-fibrosis';

@NgModule({
  declarations: [
    PaciAsociaFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(PaciAsociaFibrosisPage),
  ],
})
export class PaciAsociaFibrosisPageModule {}
