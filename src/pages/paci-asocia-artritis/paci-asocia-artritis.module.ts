import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaciAsociaArtritisPage } from './paci-asocia-artritis';

@NgModule({
  declarations: [
    PaciAsociaArtritisPage,
  ],
  imports: [
    IonicPageModule.forChild(PaciAsociaArtritisPage),
  ],
})
export class PaciAsociaArtritisPageModule {}
