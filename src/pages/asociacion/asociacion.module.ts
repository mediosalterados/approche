import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AsociacionPage } from './asociacion';

@NgModule({
  declarations: [
    AsociacionPage,
  ],
  imports: [
    IonicPageModule.forChild(AsociacionPage),
  ],
})
export class AsociacionPageModule {}
