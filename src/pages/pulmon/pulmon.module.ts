import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PulmonPage } from './pulmon';

@NgModule({
  declarations: [
    PulmonPage,
  ],
  imports: [
    IonicPageModule.forChild(PulmonPage),
  ],
})
export class PulmonPageModule {}
