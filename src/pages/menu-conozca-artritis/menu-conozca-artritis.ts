import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ArtritisPage } from '../artritis/artritis';
import { ArtristisEjerciciosPage } from '../artristis-ejercicios/artristis-ejercicios'
import { ArtristisNutricionalesPage } from '../artristis-nutricionales/artristis-nutricionales'
import { ArtritisInfografiaPage } from '../artritis-infografia/artritis-infografia'

/**
 * Generated class for the MenuConozcaArtritisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-conozca-artritis',
  templateUrl: 'menu-conozca-artritis.html',
})
export class MenuConozcaArtritisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuConozcaArtritisPage');
  }

  goConozcaEnfermedad() {
    this.navCtrl.push(ArtritisPage);
  }

  goTipsEjercicios() {
    this.navCtrl.push(ArtristisEjerciciosPage);
  }

  goNutricionales() {
    this.navCtrl.push(ArtristisNutricionalesPage);
  }

  goInfografia() {
    this.navCtrl.push(ArtritisInfografiaPage);
  }

}
