import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ENSchedulePage } from './en-schedule';

@NgModule({
  declarations: [
    ENSchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(ENSchedulePage),
  ],
})
export class ENSchedulePageModule {}
