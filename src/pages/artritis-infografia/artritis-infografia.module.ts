import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtritisInfografiaPage } from './artritis-infografia';

@NgModule({
  declarations: [
    ArtritisInfografiaPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtritisInfografiaPage),
  ],
})
export class ArtritisInfografiaPageModule {}
