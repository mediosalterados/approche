import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CIStatePage } from './ci-state';

@NgModule({
  declarations: [
    CIStatePage,
  ],
  imports: [
    IonicPageModule.forChild(CIStatePage),
  ],
})
export class CIStatePageModule {}
