import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OOIntroPage } from './oo-intro';

@NgModule({
  declarations: [
    OOIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(OOIntroPage),
  ],
})
export class OOIntroPageModule {}
