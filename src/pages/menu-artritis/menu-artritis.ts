import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {PorTiPage} from '../por-ti/por-ti';
import {ContactoPage} from '../contacto/contacto';
import {MenuConozcaArtritisPage} from '../menu-conozca-artritis/menu-conozca-artritis';
import {AcompanaPage} from '../acompana/acompana';
import {PacientesArtritisPage} from '../pacientes-artritis/pacientes-artritis';

import {ActiveUserProvider} from '../../providers/active-user/active-user';

import {ANIntroPage} from '../an-intro/an-intro';
import {ENIntroPage} from '../en-intro/en-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';
import {OOIntroPage} from '../oo-intro/oo-intro';
import {CIIntroPage} from '../ci-intro/ci-intro';

import {MyApp} from '../../app/app.component';
/**
 * Generated class for the MenuArtritisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-menu-artritis',
    templateUrl: 'menu-artritis.html',
})
export class MenuArtritisPage {

    constructor(
        @Inject(forwardRef(() => MyApp)) private app: MyApp,
        public navCtrl: NavController,
        public navParams: NavParams,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
    }

    ionViewDidLoad() {
        this.app.updatePages();
        //console.log('ionViewDidLoad MenuArtritisPage');
    }

    goPorti() {
        this.navCtrl.push(PorTiPage);
    }

    goMenuEnfermedad() {
        this.navCtrl.push(MenuConozcaArtritisPage);
    }
    goPacientes() {
        this.navCtrl.push(PacientesArtritisPage);
    }

    goContacto() {
        this.navCtrl.push(ContactoPage);
    }
    goAcompana() {
        this.navCtrl.push(AcompanaPage);
    }

    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goENIntro() {
        this.navCtrl.push(ENIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

    goOOIntro() {
        this.navCtrl.push(OOIntroPage);
    }
    goCIIntro() {
        this.navCtrl.push(CIIntroPage);
    }

}
