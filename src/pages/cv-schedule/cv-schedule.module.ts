import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CVSchedulePage } from './cv-schedule';

@NgModule({
  declarations: [
    CVSchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(CVSchedulePage),
  ],
})
export class CVSchedulePageModule {}
