import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CVThanksPage } from './cv-thanks';

@NgModule({
  declarations: [
    CVThanksPage,
  ],
  imports: [
    IonicPageModule.forChild(CVThanksPage),
  ],
})
export class CVThanksPageModule {}
