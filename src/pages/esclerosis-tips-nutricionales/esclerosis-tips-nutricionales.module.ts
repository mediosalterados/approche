import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EsclerosisTipsNutricionalesPage } from './esclerosis-tips-nutricionales';

@NgModule({
  declarations: [
    EsclerosisTipsNutricionalesPage,
  ],
  imports: [
    IonicPageModule.forChild(EsclerosisTipsNutricionalesPage),
  ],
})
export class EsclerosisTipsNutricionalesPageModule {}
