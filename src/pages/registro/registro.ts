import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, LoadingController, Keyboard} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TerminosPage} from '../terminos/terminos'
import {MenuPage} from '../menu/menu'
import {GraciasPage} from '../gracias/gracias'
import {Http} from '@angular/http';

import {NativeStorage} from '@ionic-native/native-storage/ngx';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';


/**
* Generated class for the RegistroPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
    selector: 'page-registro',
    templateUrl: 'registro.html',
})
export class RegistroPage {
    user: any;
    createSuccess = false;
    registerCredentials = {nombre: '', patologia: '', medico: '',  email: '', telefono: '', fecha_nacimiento: '', password: '', seguro: '', seguro_publico: ''};
    loading: any;
    myForm: FormGroup;

    private list: string[] = [];
    public input: string = '';
    public doctors: string[] = [];



    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        private auth: AuthServiceProvider,
        private alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        private keyboard: Keyboard,
        public http: Http,
        private nativeStorage: NativeStorage
    ) {
        this.myForm = this.createMyForm();


        this.http.get("http://165.22.227.21/WsAppRoche/web/app.php/api/user/form")
            .map(res => res.json())
            .subscribe(response => {
                //console.log(response);
                this.list = response.data.medicos;
                //console.log(this.list);
            });
    }


    public register() {

        this.auth.register(this.registerCredentials).subscribe(success => {
            if (success) {
                //this.showPopup("Success", "Account created.");
            } else {
                //this.showPopup("Error", "Problem creating account.");
            }
        },
            error => {
                // this.showPopup("Error", error);
            });
    }

    showLoader() {

        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });

        this.loading.present();

    }

    iconTxt1: string = ' d-none'
    iconTxt2: string = ' d-none'


    goIcon2() {
        this.iconTxt1 = 'd-none',
            this.iconTxt2 = 'd-none',
            this.iconTxt1 = ' '
    }
    goIcon1() {
        this.iconTxt1 = 'd-none',
            this.iconTxt2 = 'd-none',
            this.iconTxt2 = ' '
    }



    saveData() {
        var selectedPatology = this.myForm.value.patologia;
        this.showLoader();
        this.auth.register(this.registerCredentials).subscribe(success => {
            if (success && this.auth.getValidRegister()) {
                this.createSuccess = true;
                this.loading.dismiss();
                
                this.user = this.auth.getUserData();
                this.nativeStorage.setItem("currentUser", this.user); 
                
                this.goGracias(selectedPatology);

            } else {
                this.showPopup("Error", "Problem creating account.");
            }
        },
            error => {
                // this.showPopup("Error", error);
            }); 
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad RegistroPage');
    }

    private createMyForm() {
        return this.formBuilder.group({
            nombre: ['', Validators.required],
            patologia: ['', Validators.required],
            medico: ['', Validators.required],
             email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
            telefono: ['', Validators.required],
            terminos: ['', Validators.required],
            fecha_nacimiento: ['', Validators.required],
            passwordRetry: this.formBuilder.group({
                password: ['', Validators.required],
                passwordConfirmation: ['', Validators.required]

            }), seguro: ['', Validators.required],

            seguro_publico: ['', Validators.required],
        });
    }
    goTerminos() {
        this.navCtrl.push(TerminosPage)
    }

    goMenu() {
        this.navCtrl.push(MenuPage)
    }
    goGracias(selectedPatology) {
        this.navCtrl.setRoot(GraciasPage,{'patology':selectedPatology})
    }

    showPopup(title, text) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        if (this.createSuccess) {
                            this.goMenu();
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    //AUTOCOMPLETE
    add(item: string) {
        this.registerCredentials.medico = item;
        this.doctors = [];
    }

    removeFocus() {
        this.keyboard.close();
    }

    search() {
        if (!this.registerCredentials.medico.trim().length || !this.keyboard.isOpen()) {
            this.doctors = [];
            return;
        }

        this.doctors = this.list.filter(item => item.toUpperCase().includes(this.registerCredentials.medico.toUpperCase()));
    }


}
