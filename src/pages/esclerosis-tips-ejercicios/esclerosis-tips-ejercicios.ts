import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EsclerosisTipsEjerciciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esclerosis-tips-ejercicios',
  templateUrl: 'esclerosis-tips-ejercicios.html',
})
export class EsclerosisTipsEjerciciosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  className: string = 'b-modal d-none'

  ionViewDidLoad() {
    console.log('ionViewDidLoad EsclerosisTipsEjerciciosPage');
  }

  goHanldeClick() {
    this.className = 'b-modal'
  }

  goHanldeClickNone() {
    this.className = 'b-modal d-none'
  }

}
