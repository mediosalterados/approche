import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ConozcaPage} from '../conozca/conozca';
import {MenuPage} from '../menu/menu';
import {RegistroPage} from '../registro/registro';
import {LoginPage} from '../login/login';
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController) {

    }

    pushPage() {
        this.navCtrl.push(ConozcaPage);
    }

    goMenu(): void {
        this.navCtrl.push(MenuPage);
    }
    goRegistro(): void {
        this.navCtrl.push(RegistroPage);
    }
    goLogin(): void {
        this.navCtrl.push(LoginPage);
    }


}
