import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ENThanksPage } from './en-thanks';

@NgModule({
  declarations: [
    ENThanksPage,
  ],
  imports: [
    IonicPageModule.forChild(ENThanksPage),
  ],
})
export class ENThanksPageModule {}
