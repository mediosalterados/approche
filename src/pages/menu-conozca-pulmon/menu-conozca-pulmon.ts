import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PulmonPage } from '../pulmon/pulmon'
import { PulmonEjerciciosPage } from '../pulmon-ejercicios/pulmon-ejercicios'
import { PulmonNutricionPage } from '../pulmon-nutricion/pulmon-nutricion'
import { PulmonInfografiaPage } from '../pulmon-infografia/pulmon-infografia'

/**
 * Generated class for the MenuConozcaPulmonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-conozca-pulmon',
  templateUrl: 'menu-conozca-pulmon.html',
})
export class MenuConozcaPulmonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuConozcaPulmonPage');
  }

  goConozcaEnfermedad() {
    this.navCtrl.push(PulmonPage);
  }

  goTipsEjercicios () {
    this.navCtrl.push(PulmonEjerciciosPage);
  }

  goNutricionales() {
    this.navCtrl.push(PulmonNutricionPage);
  }

  goInfografia() {
    this.navCtrl.push(PulmonInfografiaPage);

  }


}
