import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PacientesAnemiaPage } from './pacientes-anemia';

@NgModule({
  declarations: [
    PacientesAnemiaPage,
  ],
  imports: [
    IonicPageModule.forChild(PacientesAnemiaPage),
  ],
})
export class PacientesAnemiaPageModule {}
