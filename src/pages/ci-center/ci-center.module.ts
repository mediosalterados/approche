import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CICenterPage } from './ci-center';

@NgModule({
  declarations: [
    CICenterPage,
  ],
  imports: [
    IonicPageModule.forChild(CICenterPage),
  ],
})
export class CICenterPageModule {}
