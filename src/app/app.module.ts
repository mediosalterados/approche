import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {ConozcaPageModule} from '../pages/conozca/conozca.module';
import {MenuPageModule} from '../pages/menu/menu.module';
import {MenuEsclerosisPageModule} from '../pages/menu-esclerosis/menu-esclerosis.module';
import {MenuAnemiaPageModule} from '../pages/menu-anemia/menu-anemia.module';
import {MenuHemofiliaPageModule} from '../pages/menu-hemofilia/menu-hemofilia.module';
import {MenuFibrosisPageModule} from '../pages/menu-fibrosis/menu-fibrosis.module';
import {MenuConozcaEsclerosisPageModule} from '../pages/menu-conozca-esclerosis/menu-conozca-esclerosis.module';
import {MenuConozcaAnemiaPageModule} from '../pages/menu-conozca-anemia/menu-conozca-anemia.module';
import {MenuConozcaFibrosisPageModule} from '../pages/menu-conozca-fibrosis/menu-conozca-fibrosis.module';
import {MenuArtritisPageModule} from '../pages/menu-artritis/menu-artritis.module';
import {MenuConozcaArtritisPageModule} from '../pages/menu-conozca-artritis/menu-conozca-artritis.module';
import {MenuConozcaHemofoliaPageModule} from '../pages/menu-conozca-hemofolia/menu-conozca-hemofolia.module';
import {EsclerosisSistemaNerviosoPageModule} from '../pages/esclerosis-sistema-nervioso/esclerosis-sistema-nervioso.module';
import {EsclerosisTipsEjerciciosPageModule} from '../pages/esclerosis-tips-ejercicios/esclerosis-tips-ejercicios.module';
import {EsclerosisTipsNutricionalesPageModule} from '../pages/esclerosis-tips-nutricionales/esclerosis-tips-nutricionales.module';
import {PorTiPageModule} from '../pages/por-ti/por-ti.module';
import {FrasesEsclerosisPageModule} from '../pages/frases-esclerosis/frases-esclerosis.module';
import {ContactoPageModule} from '../pages/contacto/contacto.module';
import {AcompanaPageModule} from '../pages/acompana/acompana.module';
import {PacientesPageModule} from '../pages/pacientes/pacientes.module';
import {PacientesAnemiaPageModule} from '../pages/pacientes-anemia/pacientes-anemia.module';
import {PacientesArtritisPageModule} from '../pages/pacientes-artritis/pacientes-artritis.module';
import {PacientesFibrosisPageModule} from '../pages/pacientes-fibrosis/pacientes-fibrosis.module';
import {PacientesHemofiliaPageModule} from '../pages/pacientes-hemofilia/pacientes-hemofilia.module';
import {PaciAsociaPageModule} from '../pages/paci-asocia/paci-asocia.module';
import {PaciAsociaAnemiaPageModule} from '../pages/paci-asocia-anemia/paci-asocia-anemia.module';
import {PaciAsociaArtritisPageModule} from '../pages/paci-asocia-artritis/paci-asocia-artritis.module';
import {PaciAsociaFibrosisPageModule} from '../pages/paci-asocia-fibrosis/paci-asocia-fibrosis.module';
import {PaciAsociaHemofoliaPageModule} from '../pages/paci-asocia-hemofolia/paci-asocia-hemofolia.module';
import {InfografiaPageModule} from '../pages/infografia/infografia.module';
import {InfografiaAnemiaPageModule} from '../pages/infografia-anemia/infografia-anemia.module';
import {InfografiaHemofiliaPageModule} from '../pages/infografia-hemofilia/infografia-hemofilia.module';
import {InfografiaFibrosisPageModule} from '../pages/infografia-fibrosis/infografia-fibrosis.module';
import {ConozcaAnemiaPageModule} from '../pages/conozca-anemia/conozca-anemia.module';
import {ConozcaHemofoliaPageModule} from '../pages/conozca-hemofolia/conozca-hemofolia.module';
import {ConozcaFibrosisPageModule} from '../pages/conozca-fibrosis/conozca-fibrosis.module';
import {EjercicioFibrosisPageModule} from '../pages/ejercicio-fibrosis/ejercicio-fibrosis.module';
import {EjercicioHemofiliaPageModule} from '../pages/ejercicio-hemofilia/ejercicio-hemofilia.module';
import {NutricionFibrosisPageModule} from '../pages/nutricion-fibrosis/nutricion-fibrosis.module';
import {NutricionHemofiliaPageModule} from '../pages/nutricion-hemofilia/nutricion-hemofilia.module';
import {ArtritisInfografiaPageModule} from '../pages/artritis-infografia/artritis-infografia.module';

import {CalendarModule} from "ion2-calendar";

import {ANIntroPageModule} from '../pages/an-intro/an-intro.module';
import {ANCalendarPageModule} from '../pages/an-calendar/an-calendar.module';
import {ANSchedulePageModule} from '../pages/an-schedule/an-schedule.module';
import {ANThanksPageModule} from '../pages/an-thanks/an-thanks.module';

import {ENIntroPageModule} from '../pages/en-intro/en-intro.module';
import {ENCalendarPageModule} from '../pages/en-calendar/en-calendar.module';
import {ENSchedulePageModule} from '../pages/en-schedule/en-schedule.module';
import {ENThanksPageModule} from '../pages/en-thanks/en-thanks.module';

import {CVIntroPageModule} from '../pages/cv-intro/cv-intro.module';
import {CVCalendarPageModule} from '../pages/cv-calendar/cv-calendar.module';
import {CVSchedulePageModule} from '../pages/cv-schedule/cv-schedule.module';
import {CVThanksPageModule} from '../pages/cv-thanks/cv-thanks.module';

import {OOIntroPageModule} from '../pages/oo-intro/oo-intro.module';
import {OOCalendarPageModule} from '../pages/oo-calendar/oo-calendar.module';
import {OOSchedulePageModule} from '../pages/oo-schedule/oo-schedule.module';
import {OOThanksPageModule} from '../pages/oo-thanks/oo-thanks.module';

import {CIIntroPageModule} from '../pages/ci-intro/ci-intro.module';
import {CIStatePageModule} from '../pages/ci-state/ci-state.module';
import {CICenterPageModule} from '../pages/ci-center/ci-center.module';
import {CICalendarPageModule} from '../pages/ci-calendar/ci-calendar.module';
import {CISchedulePageModule} from '../pages/ci-schedule/ci-schedule.module';
import {CIThanksPageModule} from '../pages/ci-thanks/ci-thanks.module';

import {ArtritisPageModule} from '../pages/artritis/artritis.module';
import {ArtristisEjerciciosPageModule} from '../pages/artristis-ejercicios/artristis-ejercicios.module';

import {ArtristisNutricionalesPageModule} from '../pages/artristis-nutricionales/artristis-nutricionales.module';

import {MenuPulmonPageModule} from '../pages/menu-pulmon/menu-pulmon.module'
import {MenuConozcaPulmonPageModule} from '../pages/menu-conozca-pulmon/menu-conozca-pulmon.module'
import {PulmonPageModule} from '../pages/pulmon/pulmon.module'
import {PulmonEjerciciosPageModule} from '../pages/pulmon-ejercicios/pulmon-ejercicios.module'
import {PulmonNutricionPageModule} from '../pages/pulmon-nutricion/pulmon-nutricion.module'
import {PulmonInfografiaPageModule} from '../pages/pulmon-infografia/pulmon-infografia.module'
import {TerminosPageModule} from '../pages/terminos/terminos.module'
import {RegistroPageModule} from '../pages/registro/registro.module'
import {LoginPageModule} from '../pages/login/login.module'
import {GraciasPageModule} from '../pages/gracias/gracias.module'
import {TrmPageModule} from '../pages/trm/trm.module'

import {StatusBar} from '@ionic-native/status-bar/ngx';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {EmailComposer} from '@ionic-native/email-composer/ngx';
import {HttpModule} from '@angular/http';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';

import {NativeStorage} from '@ionic-native/native-storage/ngx';

import {ActiveUserProvider} from '../providers/active-user/active-user';
@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        CalendarModule,
        MenuPageModule,
        ContactoPageModule,
        MenuEsclerosisPageModule,
        MenuConozcaEsclerosisPageModule,
        PorTiPageModule,
        AcompanaPageModule,
        PacientesPageModule,
        PaciAsociaPageModule,
        FrasesEsclerosisPageModule,
        EsclerosisSistemaNerviosoPageModule,
        EsclerosisTipsEjerciciosPageModule,
        EsclerosisTipsNutricionalesPageModule,
        InfografiaPageModule,
        MenuAnemiaPageModule,
        MenuArtritisPageModule,
        PacientesAnemiaPageModule,
        PaciAsociaAnemiaPageModule,
        MenuConozcaAnemiaPageModule,
        ConozcaAnemiaPageModule,
        InfografiaAnemiaPageModule,
        MenuConozcaArtritisPageModule,
        ArtritisPageModule,
        ArtristisEjerciciosPageModule,
        ArtristisNutricionalesPageModule,
        MenuFibrosisPageModule,
        MenuConozcaFibrosisPageModule,
        EjercicioFibrosisPageModule,
        NutricionFibrosisPageModule,
        InfografiaFibrosisPageModule,
        ConozcaFibrosisPageModule,
        PacientesFibrosisPageModule,
        PaciAsociaFibrosisPageModule,
        MenuHemofiliaPageModule,
        PacientesHemofiliaPageModule,
        PaciAsociaHemofoliaPageModule,
        MenuConozcaHemofoliaPageModule,
        ConozcaHemofoliaPageModule,
        EjercicioHemofiliaPageModule,
        NutricionHemofiliaPageModule,
        InfografiaHemofiliaPageModule,
        PacientesArtritisPageModule,
        PaciAsociaArtritisPageModule,
        ArtritisInfografiaPageModule,
        ConozcaPageModule,
        MenuPulmonPageModule,
        MenuConozcaPulmonPageModule,
        PulmonPageModule,
        PulmonEjerciciosPageModule,
        PulmonNutricionPageModule,
        PulmonInfografiaPageModule,
        TerminosPageModule,
        RegistroPageModule,
        LoginPageModule,
        GraciasPageModule,
        TrmPageModule,
        HttpModule,
        ANIntroPageModule,
        ANCalendarPageModule,
        ANSchedulePageModule,
        ANThanksPageModule,
        ENIntroPageModule,
        ENCalendarPageModule,
        ENSchedulePageModule,
        ENThanksPageModule,
        CVIntroPageModule,
        CVCalendarPageModule,
        CVSchedulePageModule,
        CVThanksPageModule,
        OOIntroPageModule,
        OOCalendarPageModule,
        OOSchedulePageModule,
        OOThanksPageModule,
        CIIntroPageModule,
        CIStatePageModule,
        CICenterPageModule,
        CICalendarPageModule,
        CISchedulePageModule,
        CIThanksPageModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        EmailComposer,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        AuthServiceProvider,
        NativeStorage,
        ActiveUserProvider
    ]
})
export class AppModule {}
