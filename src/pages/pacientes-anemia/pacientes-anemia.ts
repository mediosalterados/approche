 import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaciAsociaAnemiaPage } from '../paci-asocia-anemia/paci-asocia-anemia';

/**
 * Generated class for the PacientesAnemiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pacientes-anemia',
  templateUrl: 'pacientes-anemia.html',
})
export class PacientesAnemiaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacientesAnemiaPage');
  }
    goPaciAsocia(){
    this.navCtrl.push(PaciAsociaAnemiaPage);
}

}
