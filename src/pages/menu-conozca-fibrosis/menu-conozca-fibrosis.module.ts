import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuConozcaFibrosisPage } from './menu-conozca-fibrosis';

@NgModule({
  declarations: [
    MenuConozcaFibrosisPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuConozcaFibrosisPage),
  ],
})
export class MenuConozcaFibrosisPageModule {}
