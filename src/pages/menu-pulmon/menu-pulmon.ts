import {Component, Inject, forwardRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {MenuConozcaPulmonPage} from '../menu-conozca-pulmon/menu-conozca-pulmon'
import {PorTiPage} from '../por-ti/por-ti';
import {PacientesAnemiaPage} from '../pacientes-anemia/pacientes-anemia';
import {ContactoPage} from '../contacto/contacto';
import {AcompanaPage} from '../acompana/acompana';

import {ActiveUserProvider} from '../../providers/active-user/active-user';

import {ANIntroPage} from '../an-intro/an-intro';
import {CVIntroPage} from '../cv-intro/cv-intro';

import {MyApp} from '../../app/app.component';
/**
 * Generated class for the MenuPulmonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-menu-pulmon',
    templateUrl: 'menu-pulmon.html',
})
export class MenuPulmonPage {

    constructor(
        @Inject(forwardRef(() => MyApp)) private app: MyApp,
        public navCtrl: NavController,
        public navParams: NavParams,
        @Inject(forwardRef(() => ActiveUserProvider)) private activeUser: ActiveUserProvider
    ) {
        this.activeUser.getActiveUser();
    }

    ionViewDidLoad() {
        this.app.updatePages();
        //console.log('ionViewDidLoad MenuPulmonPage');
    }

    goMenuEnfermedad() {
        this.navCtrl.push(MenuConozcaPulmonPage);
    }

    goPorti() {
        this.navCtrl.push(PorTiPage);
    }

    goPacientes() {
        this.navCtrl.push(PacientesAnemiaPage);
    }

    goContacto() {
        this.navCtrl.push(ContactoPage);
    }

    goAcompana() {
        this.navCtrl.push(AcompanaPage);
    }

    goANIntro() {
        this.navCtrl.push(ANIntroPage);
    }
    goCVIntro() {
        this.navCtrl.push(CVIntroPage);
    }

}
