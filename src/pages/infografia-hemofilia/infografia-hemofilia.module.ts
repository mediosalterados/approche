import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfografiaHemofiliaPage } from './infografia-hemofilia';

@NgModule({
  declarations: [
    InfografiaHemofiliaPage,
  ],
  imports: [
    IonicPageModule.forChild(InfografiaHemofiliaPage),
  ],
})
export class InfografiaHemofiliaPageModule {}
