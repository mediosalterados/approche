import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuConozcaEsclerosisPage } from './menu-conozca-esclerosis';

@NgModule({
  declarations: [
    MenuConozcaEsclerosisPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuConozcaEsclerosisPage),
  ],
})
export class MenuConozcaEsclerosisPageModule {}
