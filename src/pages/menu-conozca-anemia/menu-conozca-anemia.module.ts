import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuConozcaAnemiaPage } from './menu-conozca-anemia';

@NgModule({
  declarations: [
    MenuConozcaAnemiaPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuConozcaAnemiaPage),
  ],
})
export class MenuConozcaAnemiaPageModule {}
