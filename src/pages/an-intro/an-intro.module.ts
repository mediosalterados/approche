import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ANIntroPage } from './an-intro';

@NgModule({
  declarations: [
    ANIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(ANIntroPage),
  ],
})
export class ANIntroPageModule {}
