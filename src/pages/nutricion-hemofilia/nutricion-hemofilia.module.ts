import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutricionHemofiliaPage } from './nutricion-hemofilia';

@NgModule({
  declarations: [
    NutricionHemofiliaPage,
  ],
  imports: [
    IonicPageModule.forChild(NutricionHemofiliaPage),
  ],
})
export class NutricionHemofiliaPageModule {}
