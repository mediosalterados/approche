import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrmPage } from './trm';

@NgModule({
  declarations: [
    TrmPage,
  ],
  imports: [
    IonicPageModule.forChild(TrmPage),
  ],
})
export class TrmPageModule {}
